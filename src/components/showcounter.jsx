import React from "react";
import { useSelector } from "react-redux";

const Showcounter = () => {
  const { counter } = useSelector((state) => state.counterReducer);

  return (
    <div>
      <h1>Counter: </h1>
      <span>{counter}</span>
    </div>
  );
};

export default Showcounter;
