import "./App.css";
import Showcounter from "./components/showcounter";
import Functioncounter from "./components/functioncounter";

function App() {
  return (
    <div className="App">
      <Showcounter />
      <Functioncounter />
    </div>
  );
}

export default App;
