import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./counter.store";

export const store = configureStore({
  reducer: {
    counterReducer,
  },
});
